# Keychron Q1

![Keychron Q1](https://cdn.shopify.com/s/files/1/0059/0630/1017/t/5/assets/pf-c4a2d4d8--KeychronQ1QMKCustomMechanicalKeyboard7.jpeg?v=1628820823)

A customizable 75% keyboard.

Q1 Version 1 is based on the MCU ATmega32u4 and Q1 Version 2 is based on the MCU STM32L432KBU6.

* Keyboard Maintainer: [Keychron](https://github.com/keychron)
* Hardware Supported: Keychron Q1
* Hardware Availability: [Keychron](https://www.keychron.com)

Make example for this keyboard (after setting up your build environment):
- For Q1 Version 1 you can use the following commands:

    ```
    make keychron/q1/ansi_atmega32u4:default
    ```

- For Q1 Version 2 you can use the following commands:

    ```
    make keychron/q1/ansi_stm32l432:default
    ```

Flashing example for this keyboard:
- For Q1 Version 1 you can use the following commands:

    ```
    make keychron/q1/ansi_atmega32u4:default:flash
    ```

- For Q1 Version 2 you can use the following commands:

    ```
    make keychron/q1/ansi_stm32l432:default:flash
    ```

**Reset Key**: Hold down the key located at *K00*, commonly programmed as *Esc* while plugging in the keyboard.

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).
